#  from pqdict import pqdict
from Graph import *


def create_Graph_from_file(file_name):
    graph = Graph()
    with open(file_name) as inputfile:
        next(inputfile)
        for line in inputfile:
            graph.add_node_to_Graph(line)
    return graph

filename = 'edges.txt'
graph = create_Graph_from_file(filename)
print "The MST cost is:"
print graph.Prim(1)
