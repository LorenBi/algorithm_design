from collections import defaultdict
from pqdict import pqdict
# pqdict is a library for using heap with a key, this allows to use all the
# methods of the heap data struct with the added benefit of the dictionary
# data structure. In the algorithm it helps to reset the cost of an edge if a
# better choice is found.


class Graph:
    def __init__(self):
        self.graph = defaultdict(list)
        self.visited = set()

    def __getitem__(self, index):
        return self.graph[index]

    def get_graph(self):
        return self.graph

    def __len__(self):
        return len(self.graph)

    def add_node_to_Graph(self, line):
        line = line.split()
        start = int(line[0])
        end = int(line[1])
        # It's an undirected graph
        self.graph[start].append((end, int(line[2])))
        self.graph[end].append((start, int(line[2])))

    def show(self):
        print self.graph

    def Prim(self, start):
        self.visited.add(start)
        total_cost = 0
        frontier = self.initialize_frontier(start)
        while len(self.visited) < len(self):
            edge = frontier.popitem()
            total_cost += edge[1]
            self.visited.add(edge[0])
            self.reset_frontier(frontier, edge[0])
        return total_cost

    def initialize_frontier(self, start):
        starting_edges = {}
        for edge in self.graph[start]:
            node = edge[0]
            cost = edge[1]
            starting_edges[node] = cost
        return pqdict(starting_edges)

    def reset_frontier(self, frontier, node):
        for edge in self.graph[node]:
            if edge[0] not in self.visited:
                if edge[0] in frontier and frontier[edge[0]] > edge[1]:
                    frontier[edge[0]] = edge[1]
                elif edge[0] not in frontier:
                    frontier.additem(edge[0], edge[1])
