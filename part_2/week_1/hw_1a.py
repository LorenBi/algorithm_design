def initialize_schedule(filename, operation):
    schedule = []
    with open(filename) as inputfile:
        next(inputfile)
        for line in inputfile:
            schedule.append(prepare_job(line, operation))
    return schedule


def prepare_job(line, operation):
    job = line.split()
    weight = int(job[0])
    length = int(job[1])
    if operation == '-':
        priority = weight - length
    elif operation == '/':
        priority = float(weight) / length
    return (priority, weight, length)


def compute_scheduling_cost(schedule):
    schedule = sorted(schedule, reverse=True)
    finishing_time = 0
    cost = 0
    for job in schedule:
        finishing_time += job[2]
        weight = job[1]
        cost += finishing_time * weight
    return cost

name = "jobs.txt"
schedule = initialize_schedule(name, '-')
print 'Suboptimal solution of the problem:'
print compute_scheduling_cost(schedule)
schedule = initialize_schedule(name, '/')
print 'Optimal solution'
print compute_scheduling_cost(schedule)
