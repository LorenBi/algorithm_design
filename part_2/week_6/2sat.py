from time import time
from random import getrandbits, sample, choice
from math import log


def read_2sat(filename):
    with open(filename) as inputfile:
        next(inputfile)
        return set(tuple(map(int, line.strip().split())) for line in inputfile)


def remove_useless_constraints(constraints):
    while contains_useless_var_remove(constraints):
        pass


def contains_useless_var_remove(constraints):
    variables = set([var for constr in constraints for var in constr])
    useless_costraints = set()
    for var1, var2 in constraints:
        if -var1 not in variables or -var2 not in variables:
            useless_costraints.add((var1, var2))
            rm_useless_of(var1, var2, variables)
    if useless_costraints:
        constraints.difference_update(useless_costraints)
        return True
    else:
        return False


def rm_useless_of(var1, var2, variables):
    if -var1 not in variables and -var2 not in variables:
        variables.discard(var1)
        variables.discard(var2)
    elif -var1 not in variables:
        variables.discard(var1)
    else:
        variables.discard(var2)


def papadimitrou(constraints):
    var_set = set([abs(var) for constr in list(constraints) for var in constr])
    for j in range(int(log2(len(var_set)))):
        variables = get_initialized_variables(var_set)
        non_satified_costr = set()

        for i in range(2 * (len(var_set) ** 2)):
            find_non_satisfied(variables, constraints, non_satified_costr)
            if not len(non_satified_costr) == 0:
                make_one_constr_true(non_satified_costr, variables)
                non_satified_costr.clear()
            else:
                return 1
    return 0


def get_initialized_variables(var_set):
    variables = {}
    for var in var_set:
        variables[var] = not getrandbits(1)
        variables[-var] = not variables[var]
    return variables


def find_non_satisfied(variables, constraints, non_satified_costr):
    for var1, var2 in constraints:
        if variables[var1] or variables[var2]:
            pass
        else:
            non_satified_costr.add((var1, var2))


def make_one_constr_true(non_satified_costr, variables):
    var1, var2 = sample(non_satified_costr, 1)[0]
    if not variables[var1] or not variables[var2]:
        chosen_var = choice([var1, var2])
        variables[chosen_var] = not variables[chosen_var]
        variables[-chosen_var] = not variables[-chosen_var]

    elif not variables[var1]:
        variables[var1] = not variables[var1]
        variables[-var1] = not variables[-var1]
    else:
        variables[var2] = not variables[var2]
        variables[-var2] = not variables[-var2]


def log2(x):
    return log(x, 2)


start = time()
result = ''
for i in range(1, 7):
    print 'Number of data:', i
    constraints = read_2sat('data/2sat' + str(i) + '.txt')
    remove_useless_constraints(constraints)
    result = result + str(papadimitrou(constraints))
    print "Result for data_" + str(i) + ':', result[-1]
print '\nResult for the assingment:', result
print 'Time spent:', int(time() - start), 's'
