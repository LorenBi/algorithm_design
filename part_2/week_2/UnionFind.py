class UnionFind:

    def __init__(self):
        self.groups = {}
        self.sizes = {}
        self.number_of_groups = 0

    def make_set(self, x):
        if x not in self.groups:
            self.groups[x] = x
            self.sizes[x] = 1
            self.number_of_groups += 1

    def find(self, x):
        if self.groups[x] != x:
            self.groups[x] = self.find(self.groups[x])
        return self.groups[x]

    def union(self, x1, x2):
        parent1 = self.find(x1)
        parent2 = self.find(x2)
        if parent1 != parent2:
            self.number_of_groups -= 1
            if self.sizes[parent1] >= self.sizes[parent2]:
                self.sizes[parent1] += self.sizes[parent2]
                self.groups[parent2] = parent1
            else:
                self.sizes[parent2] += self.sizes[parent1]
                self.groups[parent1] = parent2

    def __len__(self):
        return len(self.groups)

    def give_num_groups(self):
        return self.number_of_groups

    def __contains__(self, item):
        return item in self.groups

    def show(self):
        print self.groups
