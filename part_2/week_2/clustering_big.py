from UnionFind import UnionFind


def read_nodes(filename):
    clusters = UnionFind()
    all_nodes = []
    with open(filename) as inputfile:
        next(inputfile)
        for line in inputfile:
            clusters.make_set(int(line.rstrip().replace(' ', ''), 2))
            all_nodes.append(int(line.rstrip().replace(' ', ''), 2))
    return clusters, all_nodes


def find_num_of_cluster_with_min_dist(filename, length_implicit_pos, tot_dist):
    clusters, all_nodes = read_nodes(filename)
    distances = set_binary_dist_to_int(
        create_hamming_dist(length_implicit_pos, tot_dist))
    checked = set()
    for node in all_nodes:
        checked.add(node)
        for hamm_dist in distances:
            neighbour = hamm_dist ^ node
            if neighbour in clusters and neighbour not in checked:
                clusters.union(node, neighbour)
    print clusters.number_of_groups


def create_hamming_dist(length, dist):
    if length == 0 or dist == 0:
        return ['0' * length]
    else:
        return add_one_bit(length, dist)


def add_one_bit(length, dist):
    hamm_dist = []
    positive = create_hamming_dist(length - 1, dist - 1)
    negative = create_hamming_dist(length - 1, dist)
    if positive == '':
        hamm_dist.append('1')
    if negative == '':
        hamm_dist.append('0')
    for partial_dist1 in positive:
        hamm_dist.append('1' + partial_dist1)
    for partial_dist in negative:
        hamm_dist.append('0' + partial_dist)
    return hamm_dist


def set_binary_dist_to_int(binary):
    integer = []
    for i in binary:
        integer.append(int(i, 2))
    return integer


clust = 'clustering_big.txt'
find_num_of_cluster_with_min_dist(clust, 24, 2)
