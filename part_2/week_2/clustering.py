from UnionFind import UnionFind


def read_graph_edges(filename):
    edges = []
    with open(filename) as inputfile:
        next(inputfile)
        for line in inputfile:
            edge = map(int, line.split())
            edges.append((edge[2], edge[0], edge[1]))
    return edges


def compute_max_clusters_dist(edges, k):
    edges = sorted(edges)
    clusters = UnionFind()
    for i in range(1, 501):
        clusters.make_set(i)
    ithEdge = 0

    while clusters.number_of_groups != k or len(clusters) != 500:
        clusters.union(edges[ithEdge][1], edges[ithEdge][2])
        ithEdge += 1

    while clusters.find(edges[ithEdge][1]) == clusters.find(edges[ithEdge][2]):
        ithEdge += 1
    return edges[ithEdge][0]


name = 'clustering1.txt'
print 'The distance is:', compute_max_clusters_dist(read_graph_edges(name), 4)
