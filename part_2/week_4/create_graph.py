from collections import defaultdict


def read_graph(filename):
    Graph = defaultdict(list)
    with open(filename) as inputfile:
        n = int(inputfile.readline().split()[0])
        for line in inputfile:
            edge = map(int, line.split())
            Graph[edge[0]].append([edge[2], edge[1]])
    # print Graph
    return Graph, n


def create_Graph_from_file(file_name):
    graph = defaultdict(list)
    with open(file_name) as inputfile:
        for line in inputfile:
            add_node_to_Graph(graph, line)
    return graph


def add_node_to_Graph(graph, line):
    line = line.split()
    start = int(line[0])
    for edge in range(1, len(line)):
        end = int(line[edge].split(',')[0])
        distance = int(line[edge].split(',')[1])
        graph[start].append([distance, end])


def invert_graph(graph, num_nodes):
    inv_graph = defaultdict(list)
    for start in range(num_nodes + 1):
        for end in graph[start]:
                inv_graph[end[1]].append([end[0], start])
    return inv_graph
