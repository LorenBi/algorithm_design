from create_graph import read_graph
from Bellman_Ford import Bellman_Ford
from dijkstra import compute_shortest_path


file_name = 'data/test1.txt'

graph, num_nodes = read_graph(file_name)
print graph
shortest_path = compute_shortest_path(graph, 1, num_nodes)

print shortest_path
