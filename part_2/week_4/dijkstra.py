from collections import defaultdict
from VisitedNodes import VisitedNodes


def compute_shortest_path(graph, first_node, num_nodes):
    visited = VisitedNodes([first_node], graph[first_node])
    print len(visited)
    shortest_path = defaultdict(int)
    shortest_path[first_node] = 0
    while len(shortest_path) != num_nodes:
        start_node = visited.pop_min_node()
        shortest_path[start_node[1]] = start_node[0]
        for end_node in graph[start_node[1]]:
            if end_node in visited and end_node[1] not in shortest_path:
                visited.impose_min_distance(end_node, start_node[0])
            elif end_node not in visited:
                distance = start_node[0] + end_node[0]
                visited.append([distance, end_node[1]])
    return shortest_path
