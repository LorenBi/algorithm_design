from create_graph import invert_graph


def Bellman_Ford(graph, start_node, num_nodes):
    subproblem = create_subproblem(start_node, num_nodes)
    inv_graph = invert_graph(graph, num_nodes)
    for i in range(1, num_nodes + 1):
        subproblem.append([])
        for node in range(num_nodes + 1):
            new_step = minimum_new_step(inv_graph, i, node, subproblem)
            subproblem[i].append(min(subproblem[i - 1][node], new_step))
    if has_negative_cycle(subproblem):
        return None
    return subproblem[-1]


def create_subproblem(start_node, num_nodes):
    subproblem = [[]]
    for node in range(num_nodes + 1):
        if node == start_node:
            subproblem[0].append(0)
        else:
            subproblem[0].append(float('Inf'))
    return subproblem


def minimum_new_step(inv_graph, i, node, subproblem):
    min_dist = float('Inf')
    for new_step in inv_graph[node]:
        if new_step[0] + subproblem[i - 1][new_step[1]] < min_dist:
            min_dist = new_step[0] + subproblem[i - 1][new_step[1]]
    return min_dist


def has_negative_cycle(subproblem):
    negative_cycle = False
    i = 0
    while i < len(subproblem) and not negative_cycle:
        if subproblem[-1][i] != subproblem[-2][i]:
            negative_cycle = True
        i += 1
    return negative_cycle
