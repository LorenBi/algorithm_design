from collections import defaultdict


def read_graph(filename):
    Graph = defaultdict(list)
    with open(filename) as inputfile:
        n = int(inputfile.readline().split()[0])
        for line in inputfile:
            edge = map(int, line.split())
            Graph[edge[0]].append([edge[1], edge[2]])
    return Graph, n


def F_W_algorithm(Graph, n):
    subproblem = create_subproblem(Graph, n)
    print n
    for k in range(1, n + 1):
        print 'k =', k
        subproblem.append([])
        for i in range(n + 1):
            subproblem[k].append([])
            for j in range(n + 1):
                subproblem[k][i].append(
                    min(subproblem[k - 1][i][j], subproblem[k - 1][i][k] +
                        subproblem[k - 1][k][j]))
                if i == j and subproblem[k][i][j] != 0:
                    print subproblem[k][i][j]
                    return None
    return min([j for i in subproblem[n] for j in i])


def create_subproblem(Graph, n):
    subproblem = [[[float('Inf') for k in xrange(n + 1)]
                   for j in xrange(n + 1)]]

    for start in range(n + 1):
        subproblem[0][start][start] = 0
        for edge in Graph[start]:
            subproblem[0][start][edge[0]] = edge[1]
    return subproblem


filename = 'data/test4.txt'
print F_W_algorithm(*read_graph(filename))
