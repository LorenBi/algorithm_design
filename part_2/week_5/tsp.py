# WARNING: This algorithm is increadibly computationally intensive. I was able
# to run it with 8Gb of RAM and an quad core intel i-5. More Ram than 8 Gb is
# suggested.

from itertools import chain, combinations
from time import time
from collections import defaultdict
from math import sqrt
from gc import collect


def read_vertex(filename):
    nodes = []
    with open(filename) as inputfile:
        next(inputfile)
        for line in inputfile:
            nodes.append(tuple(map(float, line.split())))
    return nodes


def create_graph_from_file(filename):
    edges = []
    nodes = read_vertex(filename)
    for i in range(len(nodes)):
        edges.append([])
        for node in nodes:
            edges[i].append(euclid_dist(nodes[i], node))
    return edges


def euclid_dist(position1, position2):
    dist = 0
    if len(position1) == len(position2):
        for x_i in range(len(position1)):
            dist += (position1[x_i] - position2[x_i]) ** 2
    return sqrt(dist)


def all_subsets_from(s, start=1):
    return chain(*map(lambda x: combinations(s, x), range(start, len(s) + 1)))


def initialize_problem(edges):
    old_dict = defaultdict(float)
    for node in combinations(range(1, len(edges)), 1):
        old_dict[(node, node[0])] = edges[0][node[0]]
    return [old_dict, defaultdict(float)]


def minimize_last_step(sub_problem, subset, edges, end):
    subset_no_end = tuple(get_subset_no_end(subset, end))
    min_path = float('Inf')
    for node in subset_no_end:
        if sub_problem[0][(subset_no_end, node)] + edges[node][end] < min_path:
            min_path = sub_problem[0][(subset_no_end, node)] + edges[node][end]
    return min_path


def get_subset_no_end(subset, end):
    return (node for node in subset if node != end)


def add_last_edge(sub_problem, edges):
    min_path = float('Inf')
    subset = tuple(range(1, len(edges)))
    for mid_node in subset:
        if sub_problem[1][(subset, mid_node)] + edges[0][mid_node] < min_path:
            min_path = sub_problem[1][(subset, mid_node)] + edges[0][mid_node]
    return min_path


def solve_travelling_sales_man(filename):
    edges = create_graph_from_file(filename)
    sub_problem = initialize_problem(edges)
    print sub_problem[0]
    current_length = 2
    for subset in all_subsets_from(range(1, len(edges)), 2):
        if len(subset) > current_length:
            sub_problem.pop(0)
            sub_problem.append(defaultdict(int))
            current_length += 1
            print current_length
            collect()

        for end in subset:
            sub_problem[1][(subset, end)] = minimize_last_step(
                sub_problem, subset, edges, end)

    return add_last_edge(sub_problem, edges)


start = time()
print solve_travelling_sales_man('tsp.txt')

print time() - start, 's'
