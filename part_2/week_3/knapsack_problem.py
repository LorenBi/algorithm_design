import sys


def read_knapsack(filename):
    goods = []
    with open(filename) as inputfile:
        knapsack_size = int(inputfile.readline().split()[0])
        for line in inputfile:
            goods.append(tuple(map(int, line.split())))
    return goods, knapsack_size


def get_knapsack_value(goods, knapsack_size):
    gain = []
    gain.append([])
    initialize_last_line(gain, knapsack_size, 0)
    for valuable in goods:
        gain.append([])
        initialize_last_line(gain, knapsack_size)
        for sack_step in range(knapsack_size):
            heavier_sack = sack_step - valuable[1]
            if heavier_sack >= 0:
                gain[-1][sack_step] = max(gain[-2][sack_step],
                                          gain[-2][heavier_sack] + valuable[0])
            else:
                gain[-1][sack_step] = gain[-2][sack_step]
    return gain[-1][-1]


# get_big_knapsack_value doesn't work correctly yet, I still need to fix it
def get_big_knapsack_value(goods, knapsack_size):
    prev = []
    current = []
    initialize_line(prev, knapsack_size, 0)
    percent = 0
    for valuable in goods:
        percent += float(1) / 20
        print percent, '%'
        current_becomes_previous(current, prev)
        for sack_step in range(valuable[1], knapsack_size):
            heavy = sack_step - valuable[1]
            if prev[heavy] + valuable[0] > prev[sack_step]:
                current.append((sack_step, prev[heavy] + valuable[0]))
    return prev[-1]


def memoize(f):
    cache = {}

    def g(*args):
        if args not in cache:
            cache[args] = f(*args)
        return cache[args]
    return g


@memoize
def recursive_knapsack(current_goods, knapsack_size):
    global goods_B
    if current_goods == 0 or knapsack_size == 0:
        return 0
    else:
        unchanged = recursive_knapsack(current_goods - 1, knapsack_size)
        heavy = knapsack_size - goods_B[current_goods][1]
        if heavy >= 0:
            add_to_sack = recursive_knapsack(
                current_goods - 1, heavy) + goods_B[current_goods][0]
        else:
            add_to_sack = 0
        return max(unchanged, add_to_sack)


def initialize_last_line(gain, knapsack_size, value=None):
    while len(gain[-1]) < knapsack_size:
        gain[-1].append(value)


def initialize_line(prev, knapsack_size, value=None):
    while len(prev) < knapsack_size:
        prev.append(value)


def current_becomes_previous(current, prev):
    while current:
        sack_step, value = current.pop()
        prev[sack_step] = value


sys.setrecursionlimit(10000)
global goods_B

big_list = 'knapsack_big.txt'
short_list = 'knapsack1.txt'

goods, knapsack_size = read_knapsack(short_list)
goods_B, knapsack_size_B = read_knapsack(big_list)

current_goods = len(goods_B) - 1

print 'Short list', get_knapsack_value(goods, knapsack_size)
print 'Long list', recursive_knapsack(current_goods, knapsack_size_B)
