'''
quicksort_count_comparisons implements a quicksort algorithm and counts the
number of comparisons done in the function. It is O(n*log(n)) and uses costant
memory.
'''

from random import randint


def is_median(v, median, i, j):
    return v[i] <= v[median] <= v[j] or v[j] <= v[median] <= v[i]


def choose_pivot(v, start, end, param):
    if param == 'first':
        return start
    elif param == 'last':
        return end - 1
    elif param == 'random':
        return randint(start, end - 1)
    elif param == 'median':

        # I compute the half point for odd and even cases, and fined the
        # median value.

        if (end - start) % 2 == 0:
            half = (end - start) / 2 - 1 + start
        elif (end - start) % 2 == 1:
            half = (end - start - 1) / 2 + start
        if is_median(v, half, start, end - 1):
            return half
        elif is_median(v, start, half, end - 1):
            return start
        else:
            return end - 1


def partition_around_pivot(v, index_pivot, start, end):

    # I place the pivot element in the beginning

    v[start], v[index_pivot] = v[index_pivot], v[start]
    pivot = v[start]
    split = start + 1

    # the number of comparison done in this function corrispond to the length
    # of the selected array minus 1.

    comparisons = end - start - 1

    for border in range(start + 1, end):

        # I use the split variable for separating bigger and smaller elements,
        # so when v[border] is smaller than the pivot I swap it with the first
        # bigger element and increment split.

        if v[border] < pivot:
            v[split], v[border] = v[border], v[split]
            split += 1

    # I put the pivot between the smaller and bigger elements

    v[split - 1], v[start] = v[start], v[split - 1]
    return split - 1, comparisons


def quicksort_count_comparisons(v, start, end, param):

    # I return 0 comparisons if the list is empty or just one element.

    if end - start == 1 or end - start == 0:
        return 0

    index_pivot = choose_pivot(v, start, end, param)
    index_pivot, pivot_comparison = partition_around_pivot(
        v, index_pivot, start, end)

    # I solve the problem recursivly by working on the smaller and
    # bigger elements of the pivot

    left_comparisons = quicksort_count_comparisons(
        v, start, index_pivot, param)
    right_comparisons = quicksort_count_comparisons(
        v, index_pivot + 1, end, param)

    # I return the total comparison: the ones from partitioning around the
    # pivot, the ones from the recursive callings.

    tot_comparison = pivot_comparison + left_comparisons + right_comparisons
    return tot_comparison
