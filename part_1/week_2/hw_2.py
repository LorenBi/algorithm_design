from quicksort_count_comparisons import quicksort_count_comparisons


name = 'QuickSort.txt'
array = []

with open(name) as inputfile:
    for line in inputfile:
        array.append(int(line.strip()))

print "With first-method:"
print quicksort_count_comparisons(array, 0, len(array), 'median'), '\n'
