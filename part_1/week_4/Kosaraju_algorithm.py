from collections import defaultdict
from Node import *
from Stack import *
from collections import Counter


def get_SCC_sizes(name, NUMBER_OF_SCC_SIZES):
    # We first solve on the reversed Graph for the finishing time and
    # then use the result to compute the leaders
    revGraph, nodes_list = read_revGraph_create_nodes(name)
    DFS_loop(revGraph, nodes_list)
    Graph = read_change_Graph(name, nodes_list)
    DFS_loop(Graph, nodes_list)

    counter = create_counter_list(nodes_list, NUMBER_OF_SCC_SIZES)
    if NUMBER_OF_SCC_SIZES > len(counter):
        NUMBER_OF_SCC_SIZES = len(counter)

    return create_SCC_sizes(counter, NUMBER_OF_SCC_SIZES)


def read_revGraph_create_nodes(name):
    revGraph = defaultdict(list)
    max_node = 0
    with open(name) as inputfile:
        for line in inputfile:
            start, end = get_start_end_node(line)
            revGraph[end].append(start)
            if max_node < start or max_node < end:
                max_node = max(start, end)
    nodes_list = create_nodes_list(max_node)
    return revGraph, nodes_list


def create_nodes_list(max_node):
    nodes_list = [None]
    for i in range(max_node):
        c = Node()
        nodes_list.append(c)
    return nodes_list


def get_start_end_node(line):
    edge = line.split()
    start = int(edge[0])
    end = int(edge[1])
    return start, end


def reset_nodes_list(nodes_list):
    for node in range(1, len(nodes_list)):
        nodes_list[node].reset()

# In 'read_change_Graph' e use the file 'name' for creating the Graph with
# the finishing times of 'nodes_list'


def read_change_Graph(name, nodes_list):
    Graph = defaultdict(list)
    with open(name) as inputfile:
        for line in inputfile:
            start, end = get_start_end_node(line)
            new_start = nodes_list[start].get_finishing_time()
            new_end = nodes_list[end].get_finishing_time()
            Graph[new_start].append(new_end)
    reset_nodes_list(nodes_list)
    return Graph


def DFS_loop(Graph, nodes_list):
    global time
    time = 0
    global source_node
    for node in range(len(nodes_list) - 1, 0, -1):
        if nodes_list[node].get_not_explored():
            source_node = node
            DFS_iterative(Graph, node, nodes_list)


def DFS_iterative(Graph, node, nodes_list):
    global time
    stack = Stack()
    stack.push(node)
    while not stack.isEmpty():
        nodes_list[node].explore()
        nodes_list[node].set_leader(source_node)
        node = stack.peek()
        if len(Graph[node]) != 0:
            linked_node = Graph[node].pop()
            if nodes_list[linked_node].get_not_explored():
                stack.push(linked_node)
        else:
            time += 1
            nodes_list[stack.pop()].set_time(time)


def create_counter_list(nodes_list, NUMBER_OF_SCC_SIZES):
    leaders = []
    for i in range(1, len(nodes_list)):
        leaders.append(nodes_list[i].get_leader())
    return Counter(leaders).most_common(NUMBER_OF_SCC_SIZES)


def create_SCC_sizes(counter, NUMBER_OF_SCC_SIZES):
    SCC_sizes = []
    for i in range(NUMBER_OF_SCC_SIZES):
        SCC_sizes.append(counter[i][1])
    return SCC_sizes
