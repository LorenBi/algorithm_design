class Node:
    def __init__(self):
        self.__leader = None
        self.__finishing_time = None
        self.__not_explored = True

    def explore(self):
        self.__not_explored = False

    def set_time(self, current_time):
        self.__finishing_time = current_time

    def set_leader(self, leader):
        self.__leader = leader

    def get_not_explored(self):
        return self.__not_explored

    def get_finishing_time(self):
        return self.__finishing_time

    def get_leader(self):
        return self.__leader

    def reset(self):
        self.__leader = None
        self.__finishing_time = None
        self.__not_explored = True
