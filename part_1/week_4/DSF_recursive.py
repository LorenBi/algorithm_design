# 'DSF' is implemented using recursion instead of iteration. I left it because
# it is more elegant, but it causes ploblem if the Graph is really big

def DFS(Graph, node, nodes_list):
    global time
    nodes_list[node].explore()
    nodes_list[node].set_leader(source_node)
    for connected_node in Graph[node]:
        if nodes_list[connected_node].get_not_explored():
            DFS(Graph, connected_node, nodes_list)
    time += 1
    nodes_list[node].set_time(time)

# if you want to use the recursive version of DFS in "DFS_loop" you need to
# add the four lines below and substitute it to DSF_iterative
# import sys
# import resource
# sys.setrecursionlimit(10 ** 6)
# resource.setrlimit(resource.RLIMIT_STACK, (2 ** 29, 2 ** 30))
