from Kosaraju_algorithm import get_SCC_sizes

# variables connected with the problem you want to solve:
name = 'SCC.txt'
NUMBER_OF_SCC_SIZES = 5
# 'name' is the file's name, 'NUMBER_OF_SCC_SIZES' is the number of cluster
# of which you want to know the size.


print get_SCC_sizes(name, NUMBER_OF_SCC_SIZES)
