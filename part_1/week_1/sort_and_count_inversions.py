'''
The function 'sort_and_count_inversions' takes an array and returns it sorted
and how many inversions there are. The algorithm used is O(n * log(n)).
'''


def is_right_smaller(sort_right, sort_left, right_j, left_i):
    return sort_right[right_j] < sort_left[left_i]


def is_right_bigger(sort_right, sort_left, right_j, left_i):
    return sort_right[right_j] >= sort_left[left_i]


def is_list_ended(lenght, i):
    return lenght == i


def merge_and_count_split_inversions(sort_left, sort_right):
    len_left = len(sort_left)
    len_right = len(sort_right)

    left_i = 0
    right_j = 0
    inv_split = 0
    sorted_all = []

    while len_left + len_right != len(sorted_all):

        # I check if the element selected in the sort_right list is smaller
        # than the element in the sort_left, if yes, I place it in the next
        # available spot in sorted_all, increment the index for the right list
        # and add a number of inversions equals the number of the remaining
        # elements in the left array. If no I place the left element and
        # increment the left index.

        if is_right_smaller(sort_right, sort_left, right_j, left_i):
            sorted_all.append(sort_right[right_j])
            inv_split += len_left - left_i
            right_j += 1
            if is_list_ended(len_right, right_j):
                sorted_all.extend(sort_left[left_i:len_left])

        elif is_right_bigger(sort_right, sort_left, right_j, left_i):
            sorted_all.append(sort_left[left_i])
            left_i += 1
            if is_list_ended(len_left, left_i):
                sorted_all.extend(sort_right[right_j:len_right])

    return sorted_all, inv_split


def sort_and_count_inversions(v):
    if len(v) == 1:
        return v, 0

    end = len(v)
    half = end / 2
    v_left = v[0:half]
    v_right = v[half: end]

    # I use recursion for solving the problem and than I merge the
    # two arrays and count the inversions between them.

    sort_left, inv_left = sort_and_count_inversions(v_left)
    sort_right, inv_right = sort_and_count_inversions(v_right)
    sort_v, inv_split = merge_and_count_split_inversions(sort_left,
                                                         sort_right)

    total_inversions = inv_left + inv_right + inv_split
    return sort_v, total_inversions
