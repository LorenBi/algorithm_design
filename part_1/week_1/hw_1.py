from sort_and_count_inversions import sort_and_count_inversions

array = []
with open('IntegerArray.txt') as inputfile:
    for line in inputfile:
        array.append(int(line.strip()))

print sort_and_count_inversions(array)
print len(array)
