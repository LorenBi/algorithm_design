from find_min_cut import find_min_cut

name = 'kargerMinCut.txt'

Graph = []

with open(name) as inputfile:
    for line in inputfile:

        line = line.split()
        vertice = [int(j) for j in line]
        Graph.append(vertice[1:len(vertice)])

print find_min_cut(Graph, 40)
