from numpy import inf


class VisitedNodes:
    def __init__(self, source_node, next_nodes):
        self.visited = set(source_node)
        self.nodes = next_nodes
        for visited_nodes in next_nodes:
            self.visited.add(visited_nodes[1])

    def __contains__(self, item):
        return item[1] in self.visited

    def __getitem__(self, index):
        return self.nodes[index]

    def append(self, dist_node):
        self.nodes.append(dist_node)
        self.visited.add(dist_node[1])

    def extend(self, new_nodes):
        for each in new_nodes:
            self.append(each)

    def pop_min_node(self):
        min_dist = inf
        min_index = None
        for i in range(len(self.nodes)):
            if self[i][0] < min_dist:
                min_dist = self[i][0]
                min_index = i
        return self.nodes.pop(min_index)

    def impose_min_distance(self, node_edge_distance, path_distance):
        found = False
        new_distace = node_edge_distance[0] + path_distance
        i = 0
        while not found:
            if self[i][1] == node_edge_distance[1]:
                found = True
            else:
                i += 1
        if self[i][0] > new_distace:
            self[i][0] = new_distace

    def __len__(self):
        return len(self.visited)
