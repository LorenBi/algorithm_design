from collections import defaultdict
from VisitedNodes import VisitedNodes


class Graph:
    def __init__(self):
        self.graph = defaultdict(list)

    def __getitem__(self, index):
        return self.graph[index]

    def get_graph(self):
        return self.graph

    def __len__(self):
        return len(self.graph)

    def add_node_to_Graph(self, line):
        line = line.split()
        start = int(line[0])
        for edge in range(1, len(line)):
            end = int(line[edge].split(',')[0])
            distance = int(line[edge].split(',')[1])
            self.graph[start].append([distance, end])

    def compute_shortest_path(self, first_node):
        visited = VisitedNodes([first_node], self[first_node])
        shortest_path = defaultdict(float)
        shortest_path[first_node] = 0
        while len(shortest_path) != len(self):
            start_node = visited.pop_min_node()
            shortest_path[start_node[1]] = start_node[0]
            for end_node in self[start_node[1]]:
                if end_node in visited and end_node[1] not in shortest_path:
                    visited.impose_min_distance(end_node, start_node[0])
                elif end_node not in visited:
                    distance = start_node[0] + end_node[0]
                    visited.append([distance, end_node[1]])
        return shortest_path
