from Graph import Graph


def create_Graph_from_file(file_name):
    graph = Graph()
    with open(file_name) as inputfile:
        next(inputfile)
        for line in inputfile:
            print line
            graph.add_node_to_Graph(line)
    return graph


file_name = 'dijkstraData.txt'

graph = create_Graph_from_file(file_name)
required_nodes = [7, 37, 59, 82, 99, 115, 133, 165, 188, 197]
shortest_path = graph.compute_shortest_path(1)

result = ''

for node in required_nodes:
    result += str(shortest_path[node])
    result += ','

print result
