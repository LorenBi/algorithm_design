from heap import heap_max, heap_min


class MedianMaintaner():
    def __init__(self):
        self.upper_elements = heap_min()
        self.lower_elements = heap_max()

    def add_and_return_median(self, item):
        self.add(item)
        return self.median()

    def add(self, item):
        if len(self.upper_elements) == 0 or len(self.lower_elements) == 0:
            self.add_to_smallest(item)
        elif self.lower_elements.peek() <= item <= self.upper_elements.peek():
            self.add_to_smallest(item)
        elif item < self.lower_elements.peek():
            self.add_lower_element(item)
        elif self.upper_elements.peek() <= item:
            self.add_upper_element(item)

    def median(self):
        if len(self.lower_elements) >= len(self.upper_elements):
            return self.lower_elements.peek()
        else:
            return self.upper_elements.peek()

    def add_to_smallest(self, item):
        if len(self.lower_elements) == len(self.upper_elements):
            self.upper_elements.heappush(item)
        elif len(self.upper_elements) <= len(self.lower_elements):
            if item < self.lower_elements.peek():
                self.add_lower_element(item)
            else:
                self.upper_elements.heappush(item)
        else:
            if self.upper_elements.peek() < item:
                self.add_upper_element(item)
            else:
                self.lower_elements.heappush(item)

    def add_lower_element(self, item):
        if len(self.upper_elements) < len(self.lower_elements):
                self.upper_elements.heappush(
                    self.lower_elements.heappushpop(item))
        else:
            self.lower_elements.heappush(item)

    def add_upper_element(self, item):
        if len(self.lower_elements) < len(self.upper_elements):
                self.lower_elements.heappush(
                    self.upper_elements.heappushpop(item))
        else:
            self.upper_elements.heappush(item)
