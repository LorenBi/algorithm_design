from heapq import *


class heap_max():
    def __init__(self):
        self.heap = []

    def heappush(self, item):
        heappush(self.heap, - item)

    def heappop(self):
        return - heappop(self.heap)

    def heappushpop(self, item):
        return - heappushpop(self.heap, - item)

    def peek(self):
        return - self.heap[0]

    def __len__(self):
        return len(self.heap)


class heap_min():
    def __init__(self):
        self.heap = []

    def heappush(self, item):
        heappush(self.heap, item)

    def heappop(self):
        return heappop(self.heap)

    def heappushpop(self, item):
        return heappushpop(self.heap, item)

    def peek(self):
        return self.heap[0]

    def __len__(self):
        return len(self.heap)

if __name__ == '__main__':
    max_heap = heap_max()
    min_heap = heap_min()
    data = [1, 3, 5, 7, 9, 2, 4, 6, 8, 0]
    for item in data:
        max_heap.heappush(item)
        min_heap.heappush(item)
    sort = []
    desort = []
    for i in range(len(data)):
        sort.append(min_heap.heappop())
        desort.append(max_heap.heappop())
    print sort
    print desort
