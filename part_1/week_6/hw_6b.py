from MedianMaintaner import MedianMaintaner

MODULO = 10000
file_name = "Median.txt"
median_maintaner = MedianMaintaner()
medians = []
with open(file_name) as inputfile:
            for line in inputfile:
                new_median = median_maintaner.add_and_return_median(int(line))
                medians.append(new_median)
result = sum(medians) % MODULO
print result
