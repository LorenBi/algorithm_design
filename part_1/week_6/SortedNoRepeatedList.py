class SortedNoRepeatedList:
    def __init__(self):
        self.all_elements = set()
        self.sorted_list = []

    def __add(self, element):
        if element not in self.all_elements:
            self.all_elements.add(element)
            self.sorted_list.append(element)

    def read_from(self, filename):
        with open(filename) as inputfile:
            for line in inputfile:
                self.__add(int(line))
        self.sorted_list.sort()

    def tot_numbers_plus_element_in_range(self, element, interval):
        if interval[0] <= element * 2 <= interval[1]:
            total = -1
        else:
            total = 0
        print "hi"
        print self.len_sublist_smaller(interval[1] - element)
        print self.len_sublist_bigger(interval[0] - element)
        total += self.len_sublist_smaller(interval[1] - element) - \
            self.len_sublist_bigger(interval[0] - element)
        return total

    def len_sublist_smaller(self, top):
        return self.find_max_smaller_than(top) + 1

    def len_sublist_bigger(self, bottom):
        return self.find_min_bigger_than(bottom)

    def find_max_smaller_than(self, top, start_end=[]):
        self.initialize_start_end(start_end)
        length = start_end[1] - start_end[0]
        if length == 0:
            return start_end[0] - 1
        middle = length / 2 + start_end[0]
        if self.sorted_list[middle] < top:
            return self.find_max_smaller_than(top, [middle + 1, start_end[1]])
        elif self.sorted_list[middle] > top:
            return self.find_max_smaller_than(top, [start_end[0], middle])
        else:
            return middle

    def find_min_bigger_than(self, bottom, start_end=[]):
        self.initialize_start_end(start_end)
        length = start_end[1] - start_end[0]
        if length == 0:
            return start_end[0]
        middle = length / 2 + start_end[0]
        if self.sorted_list[middle] < bottom:
            return self.find_min_bigger_than(bottom,
                                             [middle + 1, start_end[1]])
        elif self.sorted_list[middle] > bottom:
            return self.find_min_bigger_than(bottom, [start_end[0], middle])
        else:
            return middle

    def initialize_start_end(self, start_end):
        if start_end == []:
            start_end.append(0)
            start_end.append(len(self.sorted_list))
